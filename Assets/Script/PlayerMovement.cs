using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody RB;

    public float ForwardForce = 2000f;
    public float sidewaysforce = 500f;

    void FixedUpdate()
    {
        RB.AddForce(0, 0, ForwardForce * Time.deltaTime);
        if(Input.GetKey("d"))
        {
            RB.AddForce(sidewaysforce * Time.deltaTime, 0, 0,ForceMode.VelocityChange);
        }
        if (Input.GetKey("a"))
        {
            RB.AddForce(-sidewaysforce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
       if(RB.position.y < -1f)
        {
            FindObjectOfType<GameManage>().EndGame();
        }
      
    }
}
