using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public PlayerMovement Movement;
     void OnCollisionEnter(Collision Info )
    {
        if(Info.collider.tag == "obstacl")
        {
            Movement.enabled = false;
            FindObjectOfType<GameManage>().EndGame();
        }
    }
}
