using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManage : MonoBehaviour
{
     bool GameOver = false;
        public float DelayTime = 2;
    public GameObject GameCompleteUI;
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void GameComplete()
    {
        GameCompleteUI.SetActive(true);
    }
    public void EndGame()
    {
        if (GameOver == false)
        {
            GameOver = true;
            Invoke("Restart",DelayTime);
        }
        
    }
}
